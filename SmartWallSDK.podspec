Pod::Spec.new do |spec|
  spec.name             = 'SmartWallSDK'
  spec.version          = '0.0.1'
  spec.license = { :type => 'BSD', :file => 'SmartWallSDK/license.txt' }
  spec.homepage         = 'https://doc.staging.smartwall.ai/'
  spec.authors          = { 'Junior Boaventura' => 'junior.boaventura@apptitude.ch' }
  spec.summary          = 'Inside the SmartWall, brands can display click-to-play video ads and generate interactions and engagement with readers.'
  spec.source           = { :http => "https://bitbucket.org/smartwall/smartwallsdk-ios-distribution/raw/#{spec.version}/SmartWallSDK.zip" }
  spec.platform = :ios, '14'
  spec.swift_version = '5.3'
  spec.ios.vendored_frameworks = 'SmartWallSDK/SmartWallSDK.xcframework'
  spec.documentation_url = 'https://doc.staging.smartwall.ai'
end